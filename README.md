# Simple OS

A simple operating system built with Rust, based on [this](https://os.phil-opp.com/) tutorial series.