//! The core kernel for the Simple OS operating system.

#![feature(lang_items)]
#![feature(unique)]
#![feature(const_fn)]
#![feature(ptr_internals)]
#![no_std]

#[macro_use]
extern crate bitflags;
extern crate multiboot2;
extern crate rlibc;
extern crate spin;
extern crate x86_64;

#[macro_use]
mod vga;
mod memory;

/// The entry point for the kernel; invoked by our boot-loader.
#[no_mangle]
pub extern fn kernel_main(multiboot_information_address: usize) {
	vga::clear_screen();

	let boot_info = unsafe { multiboot2::load(multiboot_information_address) };
	let memory_map_tag = boot_info.memory_map_tag()
			.expect("Memory map tag required");

	println!("Booting simple OS...");

	println!("Memory areas:");
	for area in memory_map_tag.memory_areas() {
		println!("    Start: 0x{:x}, length: 0x{:x}", area.base_addr, area.length);
	}

	let elf_sections_tag = boot_info.elf_sections_tag()
			.expect("ELF-sections tag required");

	let kernel_start = elf_sections_tag.sections().map(|s| s.addr).min().unwrap();
	let kernel_end = elf_sections_tag.sections().map(|s| s.addr + s.size).max().unwrap();

	let multiboot_start = multiboot_information_address;
	let multiboot_end = multiboot_start + (boot_info.total_size as usize);

	println!("Kernel start: 0x{:x}, end: 0x{:x}", kernel_start, kernel_end);
	println!("Kernel sections:");
	for section in elf_sections_tag.sections() {
		println!("    Address: 0x{:x}, Size: 0x{:x}, flags: 0x{:x}", section.addr, section.size, section.flags);
	}

	let mut frame_allocator = memory::AreaFrameAllocator::new(
		kernel_start as usize, kernel_end as usize,
		multiboot_start as usize, multiboot_end as usize,
		memory_map_tag.memory_areas(),
	);

	memory::remap_the_kernel(&mut frame_allocator, boot_info);
	println!("Success!");
}

#[lang = "eh_personality"]
#[no_mangle]
pub extern fn eh_personality() {}

#[lang = "panic_fmt"]
#[no_mangle]
pub extern fn panic_fmt(fmt: core::fmt::Arguments, file: &'static str, line: u32) -> ! {
	println!("\n\nPANIC in {} at line {}:", file, line);
	println!("    {}", fmt);
	loop {}
}
