//! Low-level memory management, page tables and frames.

use multiboot2::{MemoryArea, MemoryAreaIter};
use memory::paging::PhysicalAddress;

pub mod paging;

pub use self::paging::remap_the_kernel;

pub const PAGE_SIZE: usize = 4096;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Frame {
	number: usize,
}

impl Frame {
	fn containing_address(address: usize) -> Frame {
		Frame { number: address / PAGE_SIZE }
	}

	fn start_address(&self) -> PhysicalAddress {
		self.number * PAGE_SIZE
	}

	fn clone(&self) -> Frame {
		Frame { number: self.number }
	}

	pub fn range_inclusive(start: Frame, end: Frame) -> FrameIterator {
		FrameIterator { start, end }
	}
}

pub struct FrameIterator {
	start: Frame,
	end: Frame,
}

impl Iterator for FrameIterator {
	type Item = Frame;

	fn next(&mut self) -> Option<Frame> {
		if self.start <= self.end {
			let frame = self.start.clone();
			self.start.number += 1;
			Some(frame)
		} else {
			None
		}
	}
}

pub trait FrameAllocator {
	fn allocate_frame(&mut self) -> Option<Frame>;
	fn deallocate_frame(&mut self, frame: Frame);
}

pub struct AreaFrameAllocator {
	next_free_frame: Frame,
	current_area: Option<&'static MemoryArea>,
	areas: MemoryAreaIter,
	kernel_start: Frame,
	kernel_end: Frame,
	multiboot_start: Frame,
	multiboot_end: Frame,
}

impl AreaFrameAllocator {
	pub fn new(kernel_start: usize, kernel_end: usize,
						 multiboot_start: usize, multiboot_end: usize,
						 memory_areas: MemoryAreaIter) -> AreaFrameAllocator {
		let mut allocator = AreaFrameAllocator {
			next_free_frame: Frame::containing_address(0),
			current_area: None,
			areas: memory_areas,
			kernel_start: Frame::containing_address(kernel_start),
			kernel_end: Frame::containing_address(kernel_end),
			multiboot_start: Frame::containing_address(multiboot_start),
			multiboot_end: Frame::containing_address(multiboot_end),
		};
		allocator.choose_next_area();
		allocator
	}

	fn choose_next_area(&mut self) {
		self.current_area = self.areas.clone().filter(|area| {
			let address = area.base_addr + area.length - 1;
			Frame::containing_address(address as usize) >= self.next_free_frame
		}).min_by_key(|area| area.base_addr);

		if let Some(area) = self.current_area {
			let start_frame = Frame::containing_address(area.base_addr as usize);
			if self.next_free_frame < start_frame {
				self.next_free_frame = start_frame;
			}
		}
	}
}

impl FrameAllocator for AreaFrameAllocator {
	fn allocate_frame(&mut self) -> Option<Frame> {
		if let Some(area) = self.current_area {
			let frame = Frame { number: self.next_free_frame.number };

			let current_area_last_frame = {
				let address = area.base_addr + area.length - 1;
				Frame::containing_address(address as usize)
			};

			if frame > current_area_last_frame {
				self.choose_next_area();
			} else if frame >= self.kernel_start && frame <= self.kernel_end {
				self.next_free_frame = Frame {
					number: self.kernel_end.number + 1
				}
			} else if frame >= self.multiboot_start && frame <= self.multiboot_end {
				self.next_free_frame = Frame {
					number: self.multiboot_end.number + 1
				};
			} else {
				self.next_free_frame.number += 1;
				return Some(frame);
			}

			self.allocate_frame()
		} else {
			None
		}
	}

	fn deallocate_frame(&mut self, frame: Frame) {
		unimplemented!();
	}
}

