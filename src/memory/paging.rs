//! Paging support for the memory module.

use core::marker::PhantomData;
use core::ops::{Index, IndexMut};
use core::ptr::Unique;
use memory::*;
use multiboot2::BootInformation;

pub const ENTRY_COUNT: usize = 512;

pub const P4: *mut Table<Level4> = 0xffffffff_fffff000 as *mut _;

pub type PhysicalAddress = usize;
pub type VirtualAddress = usize;

#[derive(Debug, Clone, Copy)]
pub struct Page {
	number: usize,
}

impl Page {
	pub fn containing_address(address: VirtualAddress) -> Page {
		assert!(address < 0x0000_8000_0000_0000 || address >= 0xffff_8000_0000_0000,
						"Invalid address: 0x{:x}", address);
		Page { number: address / PAGE_SIZE }
	}

	fn start_address(&self) -> PhysicalAddress {
		self.number * PAGE_SIZE
	}

	fn p4_index(&self) -> usize { (self.number >> 27) & 0o777 }
	fn p3_index(&self) -> usize { (self.number >> 18) & 0o777 }
	fn p2_index(&self) -> usize { (self.number >> 9) & 0o777 }
	fn p1_index(&self) -> usize { (self.number >> 0) & 0o777 }
}

pub struct Entry(u64);

impl Entry {
	pub fn is_unused(&self) -> bool {
		self.0 == 0
	}

	pub fn set_unused(&mut self) {
		self.0 = 0;
	}

	pub fn flags(&self) -> EntryFlags {
		EntryFlags::from_bits_truncate(self.0)
	}

	pub fn pointed_frame(&self) -> Option<Frame> {
		if self.flags().contains(PRESENT) {
			Some(Frame::containing_address(
				self.0 as usize & 0x000fffff_fffff000
			))
		} else {
			None
		}
	}

	pub fn set(&mut self, frame: Frame, flags: EntryFlags) {
		assert_eq!(!0x000fffff_fffff000 & frame.start_address(), 0);
		self.0 = (frame.start_address() as u64) | flags.bits();
	}
}

bitflags! {
	pub struct EntryFlags:u64 {
		const PRESENT =         1 << 0;
  	const WRITABLE =        1 << 1;
  	const USER_ACCESSIBLE = 1 << 2;
  	const WRITE_THROUGH =   1 << 3;
  	const NO_CACHE =        1 << 4;
  	const ACCESSED =        1 << 5;
  	const DIRTY =           1 << 6;
  	const HUGE_PAGE =       1 << 7;
  	const GLOBAL =          1 << 8;
  	const NO_EXECUTE =      1 << 63;
	}
}

pub trait TableLevel {}

pub enum Level4 {}
pub enum Level3 {}
pub enum Level2 {}
pub enum Level1 {}

impl TableLevel for Level4 {}
impl TableLevel for Level3 {}
impl TableLevel for Level2 {}
impl TableLevel for Level1 {}

pub trait HierarchicalLevel: TableLevel {
	type NextLevel: TableLevel;
}

impl HierarchicalLevel for Level4 { type NextLevel = Level3; }
impl HierarchicalLevel for Level3 { type NextLevel = Level2; }
impl HierarchicalLevel for Level2 { type NextLevel = Level1; }

pub struct Table<L: TableLevel> {
	entries: [Entry; ENTRY_COUNT],
	level: PhantomData<L>,
}

impl<L> Table<L> where L: TableLevel {
	pub fn zero(&mut self) {
		for entry in self.entries.iter_mut() {
			entry.set_unused();
		}
	}
}

impl<L> Table<L> where L: HierarchicalLevel {
	pub fn next_table(&self, index: usize) -> Option<&Table<L::NextLevel>> {
		self.next_table_address(index)
				.map(|address| unsafe { &*(address as *const _) })
	}

	pub fn next_table_mut(&mut self, index: usize) -> Option<&mut Table<L::NextLevel>> {
		self.next_table_address(index)
				.map(|address| unsafe { &mut *(address as *mut _) })
	}

	pub fn next_table_create<A>(&mut self, index: usize, allocator: &mut A) -> &mut Table<L::NextLevel> where A: FrameAllocator {
		if self.next_table(index).is_none() {
			assert!(!self.entries[index].flags().contains(HUGE_PAGE), "mapping code does not support huge pages");

			let frame = allocator.allocate_frame().expect("no frames available");

			self.entries[index].set(frame, PRESENT | WRITABLE);
			self.next_table_mut(index).unwrap().zero();
		}

		self.next_table_mut(index).unwrap()
	}

	fn next_table_address(&self, index: usize) -> Option<usize> {
		let entry_flags = self[index].flags();
		if entry_flags.contains(PRESENT) && !entry_flags.contains(HUGE_PAGE) {
			let table_address = self as *const _ as usize;
			Some((table_address << 9) | (index << 12))
		} else {
			None
		}
	}
}

impl<L> Index<usize> for Table<L> where L: TableLevel {
	type Output = Entry;

	fn index(&self, index: usize) -> &Entry {
		&self.entries[index]
	}
}

impl<L> IndexMut<usize> for Table<L> where L: TableLevel {
	fn index_mut(&mut self, index: usize) -> &mut Entry {
		&mut self.entries[index]
	}
}

pub struct ActivePageTable {
	p4: Unique<Table<Level4>>,
}

impl ActivePageTable {
	pub unsafe fn new() -> ActivePageTable {
		ActivePageTable {
			p4: Unique::new_unchecked(P4),
		}
	}

	fn p4(&self) -> &Table<Level4> {
		unsafe { self.p4.as_ref() }
	}

	fn p4_mut(&mut self) -> &mut Table<Level4> {
		unsafe { self.p4.as_mut() }
	}

	pub fn with<F>(&mut self, table: &mut InactivePageTable, temporary_page: &mut TemporaryPage, f: F) where F: FnOnce(&mut ActivePageTable) {
		{
			let backup = Frame::containing_address(
				unsafe { ::x86_64::registers::control_regs::cr3().0 } as usize
			);
			let p4_table = temporary_page.map_table_frame(backup.clone(), self);

			self.p4_mut()[511].set(table.p4_frame.clone(), PRESENT | WRITABLE);
			::x86_64::instructions::tlb::flush_all();

			f(self);

			p4_table[511].set(backup, PRESENT | WRITABLE);
			::x86_64::instructions::tlb::flush_all();
		}

		temporary_page.unmap(self);
	}

	pub fn map<A>(&mut self, page: Page, flags: EntryFlags, allocator: &mut A) where A: FrameAllocator {
		let frame = allocator.allocate_frame().expect("Out of memory");
		self.map_to(page, frame, flags, allocator)
	}

	pub fn map_to<A>(&mut self, page: Page, frame: Frame, flags: EntryFlags, allocator: &mut A) where A: FrameAllocator {
		let mut p3 = self.p4_mut().next_table_create(page.p4_index(), allocator);
		let mut p2 = p3.next_table_create(page.p3_index(), allocator);
		let mut p1 = p2.next_table_create(page.p2_index(), allocator);

		assert!(p1[page.p1_index()].is_unused());
		p1[page.p1_index()].set(frame, flags | PRESENT);
	}

	pub fn identity_map<A>(&mut self, frame: Frame, flags: EntryFlags, allocator: &mut A) where A: FrameAllocator {
		let page = Page::containing_address(frame.start_address());
		self.map_to(page, frame, flags, allocator);
	}

	fn unmap<A>(&mut self, page: Page, allocator: &mut A) where A: FrameAllocator {
		assert!(self.translate(page.start_address()).is_some());

		let p1 = self.p4_mut()
				.next_table_mut(page.p4_index())
				.and_then(|p3| p3.next_table_mut(page.p3_index()))
				.and_then(|p2| p2.next_table_mut(page.p2_index()))
				.expect("Mapping code does not support huge pages");

		let frame = p1[page.p1_index()].pointed_frame().unwrap();

		p1[page.p1_index()].set_unused();

		::x86_64::instructions::tlb::flush(::x86_64::VirtualAddress(page.start_address()));

		allocator.deallocate_frame(frame);
	}

	pub fn translate(&self, virtual_address: VirtualAddress) -> Option<PhysicalAddress> {
		let offset = virtual_address % PAGE_SIZE;

		self.translate_page(Page::containing_address(virtual_address))
				.map(|frame| frame.number * PAGE_SIZE + offset)
	}

	fn translate_page(&self, page: Page) -> Option<Frame> {
		let p3 = self.p4().next_table(page.p4_index());

		let huge_page = || {
			p3.and_then(|p3| {
				let p3_entry = &p3[page.p3_index()];

				if let Some(start_Frame) = p3_entry.pointed_frame() {
					if p3_entry.flags().contains(HUGE_PAGE) {
						assert_eq!(start_Frame.number % (ENTRY_COUNT * ENTRY_COUNT), 0);
						return Some(Frame {
							number: start_Frame.number + page.p2_index() * ENTRY_COUNT + page.p1_index()
						});
					}
				}

				if let Some(p2) = p3.next_table(page.p3_index()) {
					let p2_entry = &p2[page.p2_index()];
					if let Some(start_frame) = p2_entry.pointed_frame() {
						if p2_entry.flags().contains(HUGE_PAGE) {
							assert_eq!(start_frame.number % ENTRY_COUNT, 0);
							return Some(Frame {
								number: start_frame.number + page.p1_index()
							});
						}
					}
				}

				None
			})
		};

		p3.and_then(|p3| p3.next_table(page.p3_index()))
				.and_then(|p2| p2.next_table(page.p2_index()))
				.and_then(|p1| p1[page.p1_index()].pointed_frame())
				.or_else(huge_page)
	}
}

pub struct InactivePageTable {
	p4_frame: Frame
}

impl InactivePageTable {
	pub fn new(frame: Frame, active_table: &mut ActivePageTable, temporary_page: &mut TemporaryPage) -> InactivePageTable {
		{
			let table = temporary_page.map_table_frame(frame.clone(), active_table);
			table.zero();
			table[511].set(frame.clone(), PRESENT | WRITABLE);
		}
		temporary_page.unmap(active_table);
		InactivePageTable { p4_frame: frame }
	}
}

pub struct TemporaryPage {
	page: Page,
	allocator: TinyAllocator,
}

impl TemporaryPage {
	pub fn new<A>(page: Page, allocator: &mut A) -> TemporaryPage where A: FrameAllocator {
		TemporaryPage { page, allocator: TinyAllocator::new(allocator) }
	}

	pub fn map(&mut self, frame: Frame, active_table: &mut ActivePageTable) -> VirtualAddress {
		assert!(active_table.translate_page(self.page).is_none(), "Temporary page is already mapped!");

		active_table.map_to(self.page, frame, WRITABLE, &mut self.allocator);
		self.page.start_address()
	}

	pub fn map_table_frame(&mut self, frame: Frame, active_table: &mut ActivePageTable) -> &mut Table<Level1> {
		unsafe { &mut *(self.map(frame, active_table) as *mut Table<Level1>) }
	}

	pub fn unmap(&mut self, active_table: &mut ActivePageTable) {
		active_table.unmap(self.page, &mut self.allocator)
	}
}

struct TinyAllocator([Option<Frame>; 3]);

impl TinyAllocator {
	fn new<A>(allocator: &mut A) -> TinyAllocator where A: FrameAllocator {
		let mut allocate = || allocator.allocate_frame();
		let frames = [allocate(), allocate(), allocate()];

		TinyAllocator(frames)
	}
}

impl FrameAllocator for TinyAllocator {
	fn allocate_frame(&mut self) -> Option<Frame> {
		for frame_option in &mut self.0 {
			if frame_option.is_some() {
				return frame_option.take();
			}
		}
		None
	}

	fn deallocate_frame(&mut self, frame: Frame) {
		for frame_option in &mut self.0 {
			if frame_option.is_none() {
				*frame_option = Some(frame);
				return;
			}
		}
		panic!("Tiny allocator can only hold 3 frames");
	}
}

pub fn remap_the_kernel<A>(allocator: &mut A, boot_info: &BootInformation) where A: FrameAllocator {
	let mut temporary_page = TemporaryPage::new(Page { number: 0xcafebabe }, allocator);

	let mut active_table = unsafe { ActivePageTable::new() };
	let mut new_table = {
		let frame = allocator.allocate_frame().expect("No more frames");
		InactivePageTable::new(frame, &mut active_table, &mut temporary_page)
	};

	active_table.with(&mut new_table, &mut temporary_page, |mapper| {
		let elf_sections_tag = boot_info.elf_sections_tag()
				.expect("Memory map tag required");

		for section in elf_sections_tag.sections() {
			assert_eq!(section.start_address() % PAGE_SIZE, 0, "sections need to be page aligned");
			println!("mapping section at addr: {:#x}, size: {:#x}", section.addr, section.size);

			let flags = paging::WRITABLE;

			let start_frame = Frame::containing_address(section.start_address());
			let end_frame = Frame::containing_address(section.end_address() - 1);

			for frame in Frame::range_inclusive(start_frame, end_frame) {
				mapper.identity_map(frame, flags, allocator);
			}
		}
	});
}